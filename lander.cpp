#include"lander.h"
#include<gl/glut.h>
#include"random.h"
#include<cmath>
#include"ground.h"

const float PI = 3.14159268;

Lander::Lander(float ix, float iy, int iradius)
{
	x = ix; y = iy; radius = iradius;
	dx=rnd(-2, 2);
	dy=rnd(1, 1);
	fuel = 300;
}

void Lander::show(){
	glPushMatrix();
	glTranslatef(x, y, 0);
	glRotatef(22, 0, 0, 1);
	glTranslatef(-x, -y, 0);
	draw();
	glPopMatrix();
}

void Lander::draw()
{
	glBegin(GL_POLYGON);		// main body of lander
	const int NPOINTS=8;
	const float TWOPI=2*3.1415927;
	const float STEP=TWOPI/NPOINTS;
	glColor3f(0, 1, 1);
	for(float angle=0; angle<TWOPI; angle+=STEP)	
		glVertex2f(x+radius*cos(angle), y+radius*sin(angle));
	glEnd();

	glBegin(GL_LINE_LOOP);		// landing gear left
	glColor3f(0, 1, 1);
	glVertex2f(x-6, y-6);
	glColor3f(0, 1, 1);
	glVertex2f(x-12, y-12);
	glEnd();

	glBegin(GL_LINE_LOOP);		// landing gear right
	glColor3f(0, 1, 1);
	glVertex2f(x, y-8);
	glColor3f(0, 1, 1);
	glVertex2f(x, y-17);
	glEnd();
}


void Lander::gravity()
{
	x += dx;			// move horizontal
		
	y += -dy;			// apply gravity
	dy += GRAVITY;		// to vertical movement
}

void Lander::incrementVel(float ddx, float ddy)
{
	if(fuel > 0)	// if there is fuel, decrement it
		{
			fuel -= 1;
			if(fuel < 1)	// if fuel is below 0, do not change it
				fuel -= 0;
		}

	dx += ddx;	// increment horizontal velocity
	dy -= ddy;	// inrecrement vertical velocity
}

void Lander::stop()	// stop the landers movement
{
	dy = dx = 0;
}

void Lander::reset() // reset position and fuel of lander
{
	x = rnd(100,900); y = rnd(350,450);
	dx=rnd(-2, 2);
	dy=rnd(1, 1);
	fuel = 300;
}

void Lander::windowcollisions()	// check for lander hitting the sides of the window, move to the opposite side if hit, to keep in view
{
	if((x+radius)<0)			// hit left border
		x = x+1000;
	else if((x-radius) > 1000)	// hit right border 
		x = x-1000;
}

float Lander::heightLeftAboveGround(Ground *g) // check height between ground and left landing gear
{
	return g->Ground::heightAt(x-12);
}

float Lander::heightRightAboveGround(Ground *g) // check height between ground and right landing gear 
{
	return g->heightAt(x);
}







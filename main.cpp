/*
	Lunar Lander game for 2D Graphics Programming Module (COMP07029)
	by B00221736

	Created in 2012, last edited in April 2012.

	Random.h/.cpp & Bitmap.h/.cpp - created by Alistair McMonnies - taken from the Blackboard site for the module.

	The background image for the game was taken from - http://media.photobucket.com/image/star%20background/sith_l0rd/star-background.png?o=125

	The function drawString() created by Alistair Mcmonnies - taken from chapter 9 of the module notes.

	The function std::string IntToStr(int n) - Taken from http://www.cplusplus.com/forum/beginner/3031/#msg12337 by the user 'Duoas'

*/

#include<iostream>
#include<gl/glut.h>
#include"random.h"
#include"lander.h"
#include"ground.h"
#include<sstream>	
#include<stdexcept>		
#include<string>
#include"Bitmap.h"

using namespace std;

float windowW = 1000, windowH = 500; float aspect; bool clearScreen; // VALUES FOR SCREEN
float startx = rnd(100,900); float starty = rnd(350,450); int startradius = 10; // VALUES TO CONSTRUCT LANDER

Ground *theground = NULL;	// create pointer for the ground
Bitmap *background = NULL;	// create pointer for background image

Lander lander(startx, starty, startradius);		// create the lander

float gametime = 0;		// setup variables
string thetime(" ");	// used to give
string thefuel(" ");	// status messages
string altitude(" ");	// on screen

void init()	// initialize the ground and background image objects
{
	theground = new Ground;
	background = new Bitmap("background.bmp", false);
}

void drawString(void *font, float x, float y, const char *str)		// Draws strings onto the OpenGL screen
{																	
	char *ch;														
	glRasterPos3f(x, y, 0.0);										
	for(ch=(char*)str; *ch ; ch++)									
		glutBitmapCharacter(font, (int)*ch);						
}																

std::string IntToStr(int n)		////////   
{									// 
  std::ostringstream result;		// 
  result << n;						// 
  return result.str();				// . It converts integers to strings
}									////////

void statusmsg()	// function for status messages on screen
{
	if(lander.getDy() != 0) // increment game time while lander is moving
		gametime += 0.002f;

	thefuel = IntToStr(lander.getfuel());		// convert these
	thetime = IntToStr((int)gametime);			// variables
	altitude = IntToStr((int)lander.gety());	// to be printed on screen as strings

	glColor3f(1, 1, 0);
	drawString(GLUT_BITMAP_HELVETICA_12, 20, 20, "Fuel: ");				// show fuel name
	drawString(GLUT_BITMAP_HELVETICA_12, 50, 20, thefuel.c_str());		// show fuel value

	drawString(GLUT_BITMAP_HELVETICA_12, 100, 20, "Time: ");			// show time name
	drawString(GLUT_BITMAP_HELVETICA_12, 130, 20, thetime.c_str());		// show time value

	drawString(GLUT_BITMAP_HELVETICA_12, 180, 20, "Altitude: ");		// show vspeed
	drawString(GLUT_BITMAP_HELVETICA_12, 230, 20, altitude.c_str());	// show hspeed

	if(lander.gety() > 1000)	// When lander goes too high, stop game
		{
			drawString(GLUT_BITMAP_TIMES_ROMAN_24, 100, 350, "You are lost in orbit! Press R to reset the game!");
			lander.stop();
		}
	if(lander.getfuel() < 1)	// Tell user when they have no more fuel
		drawString(GLUT_BITMAP_TIMES_ROMAN_24, 100, 250, "You have no more fuel!");
}


void collisions()
{
	if(lander.gety() - lander.heightLeftAboveGround(theground) == lander.gety() - lander.heightRightAboveGround(theground)) // check if lander is above flat area
		{
			if(((lander.gety() - lander.heightLeftAboveGround(theground)) < 16) && lander.gety() - lander.heightRightAboveGround(theground) < 16)	// tests if lander is touching the ground
				{
					lander.stop();
					drawString(GLUT_BITMAP_TIMES_ROMAN_24, 100, 250, "You have landed successfully! Press R to reset the game");
				}	
		}
	else	
		{
			if(((lander.gety() - lander.heightLeftAboveGround(theground)) < 16) && lander.gety() - lander.heightRightAboveGround(theground) < 16)	// tests if lander is touching the ground NOT above a flat area
				{
					lander.stop();
					drawString(GLUT_BITMAP_TIMES_ROMAN_24, 100, 220, "You have crashed! Press R to reset the game!");
				}
		}
}

void display(void)
{
	glClear(GL_COLOR_BUFFER_BIT);

	background->draw(0, 100, 1000, 500);
	theground->show();
	lander.show();

	collisions();	// check collisions with ground

	statusmsg();	// show status messages on screen

	glutSwapBuffers();
}


void keysdown(unsigned char key, int x, int y)		// NORMAL KEYS
{
	switch(key)
	{
		case 'a':				// LEFT
			if(lander.getfuel() > 0)			// if there is fuel, allow movement
				lander.incrementVel(-0.02, 0);
			else								// if no fuel, disallow movement, but keep lander on trajectory
				lander.incrementVel(0,0);
			break;
		case 'd':				// RIGHT
			if(lander.getfuel() > 0)
				lander.incrementVel(0.02, 0);
			else
				lander.incrementVel(0,0);
			break;
		case 'w':				// UP
			if(lander.getfuel() > 0)
				lander.incrementVel(0, 0.03);
			else
				lander.incrementVel(0,0);
			break;
		case 'r':				// Reset game
			lander.reset();
			gametime = 0;
			break;
		case 27:				// ESCAPE KEY EXIT
			exit(0);
			break;
	}
}

void special_keys(int key, int x, int y)	// ARROW KEYS
{
	switch(key) 
	{
		case GLUT_KEY_UP:					// ARROW UP
			if(lander.getfuel() > 0)
				lander.incrementVel(0, 0.03);
			else
				lander.incrementVel(0,0);
			break;
		case GLUT_KEY_RIGHT:				// ARROW RIGHT
			if(lander.getfuel() > 0)
				lander.incrementVel(0.02, 0);
			else
				lander.incrementVel(0,0);
			break;
		case GLUT_KEY_LEFT:					// ARROW LEFT
			if(lander.getfuel() > 0)
				lander.incrementVel(-0.02, 0);
			else
				lander.incrementVel(0,0);
			break;
	}
}

void timer(int value)
{
	display();

	lander.gravity();			// keep affecting lander with gravity
	lander.windowcollisions();	// check if lander leaves the window, if so, move it to the opposite side to keep it in view
	
	glutTimerFunc(20, timer, 0);
}

void main(int argc, char **argv)
{
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA);
	glutInitWindowSize((int)windowW,(int)windowH);
	glutCreateWindow("Lunar Lander");
	gluOrtho2D(0, windowW, 0, windowH);
	init();
	glClearColor(0.0, 0.0, 0.0, 1.0);
	glutDisplayFunc(display);
	glutTimerFunc(20, timer, 0);
	glutIdleFunc(display);
	glutKeyboardFunc(keysdown);
	glutSpecialFunc(special_keys);
	glutMainLoop(); 
}
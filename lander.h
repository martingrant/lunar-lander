#ifndef LANDER_H
#define LANDER_H

#include "ground.h"

// lander.h Lander Class Definition

const float GRAVITY = 0.003;

class Lander
{
	public:
		Lander(float ix, float iy, int iradius);
		void show();	
		void draw();
		void gravity();
		void windowcollisions();
		void stop();
		void incrementVel(float ddx, float ddy);
		void reset();
		int getfuel(){return fuel;}
		void setfuel(int newfuel){fuel = newfuel;}
		float gety(){return y;}
		float getx(){return x;}
		float getDx(){return dx;}
		float getDy(){return dy;}
		float heightLeftAboveGround(Ground *g);
		float heightRightAboveGround(Ground *g);
	private:
		float x, y, radius, dy, dx;
		int fuel;
};
#endif
#include"ground.h"
#include<gl/glut.h>
#include"random.h"

Ground::Ground()
{
	for(int i=0; i<100; i++){	// random heights for ground level
		heights[i] = rnd(50, 100);
	}

	int f1 = rnd(10, 50); // flat area 1
	int f2 = rnd(50, 80); // flat area 2

	float h = heights[f1];

	for(int i=f1; i<f1+10; i++){	// insert flat area 1
		heights[i] = h;
	}

	h = heights[f2];
	for(int i=f2; i<f2+10; i++){	// insert flat area 2
		heights[i] = h;
	}
}

void Ground::show()
{
	glBegin(GL_LINE_STRIP);
		glColor3f(0.5, 0.5, 0.5);
		glLineWidth(5.0);

		glVertex2f(-02, -1);
		for(int i=0; i<100; i++){
			glVertex2f(i*10, heights[i]);
		}
		glVertex2f(1015, -1);	
	glEnd();
}

